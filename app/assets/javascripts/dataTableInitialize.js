$(function(){
  $(document).ready(function(){
    $("#my-papers-table").DataTable();
    $("#my-papers-pending-table").DataTable();
    $("#not-my-papers-table").DataTable();
    $("#reviewer-paper-table").DataTable();
    $("#reviewer-paper-pending-table").DataTable();
    $("#professor-paper-pending-table").DataTable();
    $("#professor-paper-concluded-table").DataTable();
    $("#professor-paper-without-coordinator-table").DataTable();
    $("#events-table").DataTable({
      "ordering": false,
      "searching": false,
      "lengthChange": false,
      "paging": false
    });
  });  
})

