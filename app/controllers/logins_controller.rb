class LoginsController < ApplicationController
  layout 'login'
  def login
  end

  def update_login_form
    if params[:login_type] == "COORDENADOR"
      render '_form_coordinator', :layout => false
    elsif params[:login_type] == "REVISOR"
      render '_form_reviewer', :layout => false
    elsif params[:login_type] == "PROFESSOR"
      render '_form_professor', :layout => false
    end
  end
end

