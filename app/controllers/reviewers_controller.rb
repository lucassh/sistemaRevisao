class ReviewersController < ApplicationController
  before_action :authenticate_reviewer!
  before_action :find_paper, only: [:reviewer_edit, :reviewer_save]
  layout 'reviewer'
  def index
    #@reviewer_papers = Paper.select{|paper| paper.reviewer_id == current_reviewer.id}
    @reviewer_papers_onhold = current_reviewer.papers.select{|paper| paper.status!='CONCLUIDA'}
    @reviewer_papers_pending = current_reviewer.papers.select{ |paper| paper.status=="REVISOR" || paper.status=="VOLTOU REVISOR"}.size
    @reviewer_papers_concluded = current_reviewer.papers.select{ |paper| paper.status=="CONCLUIDA"}.size
  end

  def reviewer_pending_papers
    @reviewer_papers_pending = current_reviewer.papers.select{ |paper| paper.status=="REVISOR" || paper.status=="VOLTOU REVISOR"}
  end
  def reviewer_concluded_papers
    @reviewer_papers_concluded = current_reviewer.papers.select{ |paper| paper.status=="CONCLUIDA"}
  end

  def reviewer_edit
  end

  def reviewer_save

    if @paper.update(status: 'DEVOLVIDO', reviewed_file_key: params[:paper][:reviewed_file_key])
      Event.create(log: 'Devolvido para o coordenador. Revisor concluiu a revisão', paper_id: @paper.id)
      redirect_to reviewer_index_path, notice: 'Estado atualizado com sucesso.'
    else
      redirect_to reviewer_index_path, notice: 'Não foi possível atualizar o estado deste artigo'
    end
  end

  private

  def find_paper
    @paper = Paper.find(params[:paper_id])
  end
end
