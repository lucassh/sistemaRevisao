$(function(){
  
  $(".reviewer-update-wrapper").ready(function(){
    if ($("#paper_reviewed_file_key").val() == "") {
      $(".reviewer-paper-button").prop('disabled', true);
    } else {
      $(".reviewer-paper-button").prop('disabled', false);
    };
  });
  
  
  $("#paper_reviewed_file_key").change(function(){
    if ($("#paper_reviewed_file_key").val() == "") {
    $(".reviewer-paper-button").prop('disabled', true);
  } else {
    $(".reviewer-paper-button").prop('disabled', false);
    $(".reviewer-checkbox").prepend("<p><span style='color: red'>Não esquece de marcar como concluído.</span><p>");
  };
  });

});