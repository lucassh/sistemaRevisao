class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :find_paper, only: [:pdf_render]

  def pdf_render
    if params[:type_of_file] == 'original'
      @modal_header = 'Artigo Original' 
      # @file = @paper.original_file_key.file.file.split('/').last
    else
      @modal_header = 'Artigo Revisado'
      # @file = @paper.reviewed_file_key.file.file.split('/').last
    end
    render 'shared/_pdf_render', :layout => false
  end

  private

  def find_paper
    @paper = Paper.find(params[:paper_id]) if params[:paper_id]
    @paper = Paper.find(params[:id]) if params[:id]
  end

end
