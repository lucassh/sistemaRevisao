class CreatePapers < ActiveRecord::Migration[5.1]
  def change
    create_table :papers do |t|
      t.string :title
      t.string :original_file_key
      t.string :reviewed_file_key
      t.string :status
      t.references :professor, foreign_key: true
      t.references :reviewer, foreign_key: true
      t.references :coordinator, foreign_key: true

      t.timestamps
    end
  end
end
