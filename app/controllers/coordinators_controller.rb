class CoordinatorsController < ApplicationController
  before_action :authenticate_coordinator!
  before_action :find_paper, only: [:show_paper, :take_paper, :return_paper, :allocate_reviewer, :desallocate_reviewer, :concluded?, :pdf_render, :coordinator_events]
  layout 'coordinator'
  def index
    @all_papers = Paper.select{|paper| paper.coordinator_id == nil}
    # @coordinator_papers = Paper.all.where(coordinator_id: current_coordinator.id).order('updated_at DESC')
    @coordinator_papers_concluded = Paper.all.select{ |paper| paper.coordinator_id==current_coordinator.id && paper.status=='CONCLUIDA'}.size
    @coordinator_papers = Paper.all.select{ |paper| paper.coordinator_id==current_coordinator.id && paper.status!='CONCLUIDA'}.size
    @coordinator_papers_pending = Paper.select{|paper| paper.coordinator_id == current_coordinator.id && (paper.status == 'RECEBIDO' || paper.status == 'DEVOLVIDO')}.size
  end

  def coordinator_pendings_papers
    @coordinator_papers_pending = Paper.select{|paper| paper.coordinator_id == current_coordinator.id && (paper.status == 'RECEBIDO' || paper.status == 'DEVOLVIDO')}
  end
  
  def coordinator_onhold_papers
    @coordinator_papers = Paper.all.select{ |paper| paper.coordinator_id==current_coordinator.id && paper.status!='CONCLUIDA'}
  end
  
  def coordinator_concluded_papers
    @coordinator_papers_concluded = Paper.all.select{ |paper| paper.coordinator_id==current_coordinator.id && paper.status=='CONCLUIDA'}
  end

  def show_paper
    @reviewers = Reviewer.all
  end

  def coordinator_events
    @events = @paper.events 
  end

  def take_paper
    if @paper.update(coordinator_id: current_coordinator.id, status: 'RECEBIDO')
      Event.create(log: 'Recebido pelo coordenador', paper_id: @paper.id)
      redirect_to coordinator_index_path, notice: 'Artigo capturado com sucesso'
    else
      redirect_to coordinator_index_path, notice: 'Não foi possível capturar este artigo, ele escapou das suas mãos.'
    end
  end
  def return_paper
    if @paper.update(coordinator_id: nil, reviewer_id: nil, status:'SUBMETIDO')
      Event.create(log: 'Artigo devolvido pelo coordenador', paper_id: @paper.id)
      redirect_to coordinator_index_path, notice: 'Artigo devolvido para selva com sucesso'
    else
      redirect_to coordinator_index_path, notice: 'Não foi possível devolver este artigo, ele se apegou a você.'
    end
  end

  def allocate_reviewer
    if @paper.update(reviewer_id: params[:paper][:reviewer_id], status: 'REVISOR')
      Event.create(log: 'Enviado para revisor', paper_id: @paper.id)
      redirect_to coordinator_index_path, notice: 'Artigo alocado com sucesso.'
    else
      redirect_to coordinator_index_path, notice: 'Não foi possível alocar este artigo.'
    end
  end

  def desallocate_reviewer
    if @paper.update(reviewer_id: nil, status: 'RECEBIDO')
      Event.create(log: 'Recebido pelo coordenador. Coordenador removeu o revisor para o artigo' ,paper_id: @paper.id)
      redirect_to coordinator_index_path, notice: 'Revisor desalocado com sucesso.'
    else
      redirect_to coordinator_index_path, notice: 'Não foi possível desalocar este revisor.'
    end
  end

  def concluded?
    if @paper.update(status: params[:paper][:status])
      if params[:paper][:status] == 'CONCLUIDA'
        Event.create(log: 'Revisão foi aprovada. Procedimento concluído', paper_id: @paper.id)
      else
        Event.create(log: 'Revisão não foi aprovada pelo coordenador. Artigo voltou para o revisor.', paper_id: @paper.id)
      end
      redirect_to coordinator_index_path, notice: 'Artigo atualizado com sucesso.'
    else
      redirect_to coordinator_index_path, notice: 'Não foi possível atualizar este artigo.'
    end

  end
  def pdf_render
    if params[:type_of_file] == 'original'
      @modal_header = 'Artigo Original' 
      @file = @paper.original_file_key.file.file.split('/').last
    else
      @modal_header = 'Artigo Revisado'
      @file = @paper.reviewed_file_key.file.file.split('/').last
    end
    render '_pdf_render', :layout => false
  end

  private 

  def find_paper
    @paper = Paper.find(params[:paper_id])
  end
end
