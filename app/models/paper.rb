class Paper < ApplicationRecord
  has_one :professor, :foreign_key => 'id' ,
                      :primary_key => 'professor_id'
  has_one :reviewer, :foreign_key => 'id' ,
                      :primary_key => 'reviewer_id'
  
  has_one :coordinator, :foreign_key => 'id',
                        :primary_key => 'coordinator_id'
  
  has_many :events, dependent: :destroy, :foreign_key => 'paper_id',
                                         :primary_key => 'id'

  # as_one_attached :file
  mount_uploader :original_file_key, PaperUploader, dependent: :destroy
  mount_uploader :reviewed_file_key, PaperUploader, dependent: :destroy

  validates :original_file_key, presence: true
  validates :title, presence: true
end
