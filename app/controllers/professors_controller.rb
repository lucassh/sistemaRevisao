class ProfessorsController < ApplicationController
  before_action :authenticate_professor!
  before_action :find_paper, only: [:events]
  layout 'professor'

  def index
    # @professor_papers_concluded = Paper.select{|paper|  paper.professor_id == current_professor.id && paper.status == 'CONCLUIDA'}
    # @professor_papers_pending = Paper.select{|paper|  paper.professor_id == current_professor.id && paper.status != 'CONCLUIDA'}
    @professor_papers_concluded = Paper.select{|paper|  paper.professor_id == current_professor.id && paper.status == 'CONCLUIDA'}.size
    @professor_papers_pending = Paper.select{|paper|  paper.professor_id == current_professor.id && paper.status != 'CONCLUIDA'}.size
    @professor_papers = Paper.select{|paper| paper.coordinator_id == nil}
  end
  def professor_pending_papers
    @professor_papers_pending = Paper.select{|paper|  paper.professor_id == current_professor.id && paper.status != 'CONCLUIDA'}
  end
  def professor_concluded_papers
    @professor_papers_concluded = Paper.select{|paper|  paper.professor_id == current_professor.id && paper.status == 'CONCLUIDA'}
  end


  def events
    @events = @paper.events 
  end
 
  private

  def find_paper
    @paper = Paper.find(params[:paper_id])
  end
end
