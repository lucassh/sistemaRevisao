//= require rails-ujs
//= require jquery2
//= require turbolinks


$(function(){
  $("#login-content").ready(function(){
    $.ajax({
      type: "GET",
      url:"/update_login_form",
      dataType: "html",
      data: { login_type: 'COORDENADOR' },
      success: function(result){
        $("#login-content").html(result);
      }
    });
  })
  $("#login-type-select").change(function(){
    //$("body").loading();
    $.ajax({
      type: "GET",
      url:"/update_login_form",
      dataType: "html",
      data: { login_type: $("#login-type-select option:selected").val() },
      success: function(result){
        $("#login-content").html(result);
      }
    });
    //$("body").loading("stop");
    console.log("Mudou!");
  });
})